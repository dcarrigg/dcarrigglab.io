I'm a game developer from New Hampshire, USA. I have experience leading software teams, designing and developing complex game systems, and working with various networking architectures.

I've run indie game development studios, developed college-level curriculum for game development programs, and consulted on autonomous vehicles and training simulation projects.
