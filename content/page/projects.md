---
title: Projects
subtitle: A subset of the projects I've been involved with
comments: false
---
In addition to the projects listed below, I have also been involved with:

**Griftlands** - Developed by Klei Entertainment

**Republique** - By Camouflaj

**Norwood Suites** - By Alliance Metaversal Studio LLC

**Wave Break** - By Funktronic Labs

**Roxo, the FedEx SameDay Bot** - An autonomous delivery robot for FedEx, developed by DEKA Research and Development.

**A nurse training simulation** using virtual reality, for Boston Children's Hospital

**A kart-racing game** for NH Make-a-wish foundation

**A CT Scan simulation** using virtual reality, for SNHU's Psychology Department  

**Circuit Superstars** - By Original Fire Games, technical consultant for online multiplayer mechanics


PROJECTS
========

JOB SIMULATOR
-------------

A tongue-in-cheek VR launch title for HTC Vive, Oculus Touch, and PlayStation VR.

The year is 2050. In a world where robots have replaced all human jobs, step into the "Job Simulator" to learn what it was like 'to job'. Players can re-live the glory days of work by simulating the ins and outs of being a gourmet chef, an office worker, a convenience store clerk, and more.

![](https://images.squarespace-cdn.com/content/v1/56019980e4b068aeb0c61d0d/1451921107941-1QTGLF4IY8VFVAMBKP57/ke17ZwdGBToddI8pDm48kAE8HdRvRylTvU2YG9QaCZcUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcTQ7XEagTpmkvQkP8G_NFKPWkztdk8KZeiTA6zc-QlimI9eyxsZq0FtF7PM2-0kDO/image-asset.png?format=2500w)

**Role:** Gameplay Programmer

**Studio:** Owlchemy Labs

**Tech:** Unity3D, C#, HTC Vive

**Team Size:** 8-10

**Platforms:** HTC Vive, Oculus Rift, Playstation VR

<http://jobsimulatorgame.com/>


LEGEND OF DUNGEON: MASTERS
--------------------------

You are the Master of this legendary dungeon! Well.. one of them at least!\
Legend of Dungeon Masters lets you toy with people streaming the action RPG game Legend of Dungeon. Help your friends and favorite streamers on their quest for treasure, and send them mighty swords, potions, and bazookas... or don't! Rally the other viewers and laugh evilly as you rain hell upon them with beasts, kittens, and demons. Help or hinder, it's entirely up to you!

![](https://images.squarespace-cdn.com/content/v1/56019980e4b068aeb0c61d0d/1451921078075-3OKH921U9VRPU9D0WZK3/ke17ZwdGBToddI8pDm48kAE8HdRvRylTvU2YG9QaCZcUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcTQ7XEagTpmkvQkP8G_NFKPWkztdk8KZeiTA6zc-QlimI9eyxsZq0FtF7PM2-0kDO/image-asset.png?format=2500w)

**Role:** Lead Programmer

**Studio:** Robot Loves Kitty

**Tech:** Unity3D, C#, php, mySql

**Team Size:** 4-6

<http://store.steampowered.com/app/405980/>

<http://robotloveskitty.com/LoD/>

UPSILON CIRCUIT
---------------

Part game, part game show, Upsilon Circuit is an action RPG with only eight contestants playing at a time. Everything is broadcast live for the internet to watch and interact. Each contestant only has one shot, and when they die a new contestant is selected from the viewing audience.

Spectators can interact with the live game in various ways, sending in items, monsters, and influencing the action.

![screenshotmockup3.png](https://images.squarespace-cdn.com/content/v1/56019980e4b068aeb0c61d0d/1442955086828-4NPYTKZE49L59HY4ZC7Z/ke17ZwdGBToddI8pDm48kAE8HdRvRylTvU2YG9QaCZcUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcTQ7XEagTpmkvQkP8G_NFKPWkztdk8KZeiTA6zc-QlimI9eyxsZq0FtF7PM2-0kDO/screenshotmockup3.png?format=1500w)

**Role:** Lead Programmer

**Studio:** Robot Loves Kitty

**Tech:** Unity3D, C#, php, mySql

**Team Size:** 4-6

**Platforms:** Windows, Mac, Linux, Android, WebGL

[http://www.upsiloncircuit.com](http://www.upsiloncircuit.com/)


SNAPSHOT
--------

Snapshot is the tale of a lone robot lost in an abandoned world. Armed with only his trusty camera, Pic sets forth on his great adventure. A camera might not seem like enough for a puzzle platforming adventure, but this camera is different from most: It has the ability to capture and remove from the world the very objects that it photographs. Not only that, but it can also use its powers to paste the photos it took back into the environment! Everything that the camera captures is perfectly preserved, and when the photos are restored the objects are restored with it. On top of all of that, this amazing camera can also rotate the photos before they're pasted. Take a picture of an incoming fireball, rotate it and paste it to send it flying into a wall of heavy boxes to knock it out of the way. See what kind of crazy things can happen to Pic with his awesome (and probably magical) camera during the adventure of his life!

![](https://images.squarespace-cdn.com/content/v1/56019980e4b068aeb0c61d0d/1442960298669-NS7TXKPZT1HOLMKS3M1M/ke17ZwdGBToddI8pDm48kAE8HdRvRylTvU2YG9QaCZcUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcKjbqQNO1Y2NcFqmWIjbVMI71epF9k9WAv7ULL-X1b0UQYR-MkV-EFwjEbhfn5rIu/image-asset.jpeg?format=2500w)


**Role:** Founder, Lead Programmer

**Studio:** Retro Affect

**Tech:** RAE - C++, Lua, OpenGL, SDL2, OpenAL, Fmod, Box2d

**Platforms:** Windows, Mac, Linux, Consoles

**Team Size:** 3-5

<http://www.retroaffect.com/games/>

<http://store.steampowered.com/app/204220/>

<https://en.wikipedia.org/wiki/Snapshot_(video_game)>

RETRO AFFECT ENGINE (RAE)
-------------------------

The Retro Affect Engine (RAE) is the engine I developed to use on Snapshot. RAE features:\
* A fully integrated level editor - Includes tools for placing, rotating, and scaling objects as well as placing world geometry, physics joints, and more\
* Component based objects system - Objects comprised of Gibs (as in "instagib", where a guy explodes into a bunch of little pieces or "gibs")\
* Physics based gameplay - Currently using Box2d\
* Custom scripted objects and levels using Lua\
* N levels of parallax support\
* Cross platform - Windows, OSX, Linux, Vita, and PS3. PS4 and WiiU under development\
* 2d Animation system\
* Internal subscription based event system\
* Custom 2d Lighting system\
* Positional sound support - Supports OpenAL and FMod\
* ...and more...

![](https://images.squarespace-cdn.com/content/v1/56019980e4b068aeb0c61d0d/1442963000412-8SSPWAQ0067LBAMDVIMR/ke17ZwdGBToddI8pDm48kAE8HdRvRylTvU2YG9QaCZcUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcKjbqQNO1Y2NcFqmWIjbVMI71epF9k9WAv7ULL-X1b0UQYR-MkV-EFwjEbhfn5rIu/image-asset.jpeg?format=2500w)

**Role:** Lead Programmer

**Studio:** Retro Affect

**Tech:** C++, Lua, OpenGL, SDL2, OpenAL, Fmod, Box2d

**Platforms: **Windows, Mac, Linux, Vita, PS3

<http://retroaffect.com/blog/178/RAE_Day__The_Level_Editor/#b>

[http://retroaffect.com/blog/180/What_s_RAE_Wednesday__Engine_Features_and_Gibs/](http://retroaffect.com/blog/180/What_s_RAE_Wednesday__Engine_Features_and_Gibs/#b)


STARGATE WORLDS
---------------

Stargate Worlds is a MMORPG developed by Cheyenne Mountain Entertainment. Although it was never released, we brought it to a state where we were testing with beta users.

![](https://images.squarespace-cdn.com/content/v1/56019980e4b068aeb0c61d0d/1442970708101-9I947K3AFA8XSGPGFXNF/ke17ZwdGBToddI8pDm48kAE8HdRvRylTvU2YG9QaCZcUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcKjbqQNO1Y2NcFqmWIjbVMI71epF9k9WAv7ULL-X1b0UQYR-MkV-EFwjEbhfn5rIu/image-asset.jpeg?format=2500w)

**Role:** Lead Gameplay Engineer

**Studio:** Cheyenne Mountain Entertainment / Firesky

**Tech:** BigWorld MMO Server Tech, Python, C++, Unreal Engine

**Team Size:** 100+

**Platforms: **Windows

<https://en.wikipedia.org/wiki/Stargate_Worlds>
