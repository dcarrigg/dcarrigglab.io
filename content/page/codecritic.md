---
title: Code Critic
subtitle: An automatic code validation and assessment pipeline
comments: false
---



{{< youtube W_q-U6056Ks >}}

[https://www.youtube.com/watch?v=W_q-U6056Ks](https://www.youtube.com/watch?v=W_q-U6056Ks)


Code Critic is a system for automatic feedback for programming assignments, but could be expanded into the programming review processes at any software company. Code Critic provides many benefits not found in other automatic code assessment systems, including methods for generating automatic feedback for students, insights for instructors, and the potential to be expanded to meet specific technological or logistical needs for individual universities or courses.



[More information can be found here, in the full project paper](https://docs.google.com/document/d/10CuK-HOY6CaEUQ12J_ghMZ9QvV2_18wtxiib0HgSO1w/edit?usp=sharing)
