---
title: Contact
subtitle: Here's how to get in touch
comments: false
---

You can typically find me on Discord or in my office at Southern New Hampshire University.

Email: dcarrigg@gmail.com
Discord: dcarrigg#6015
