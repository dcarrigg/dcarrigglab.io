---
title: UCNetwork
subtitle: Client-Server and P2P networking libraries for Unity
comments: false
---

These are the MIT Licensed networking libraries I developed for Unity, used for Upsilon Circuit as well as many other prototype projects.

Features:
---------
- Online multiplayer!
- Real-time messaging and object synchronization for Unity3D
- Authoritative, dedicated, and standalone server support
- Support for clients with different codebases, and asymmetrical experiences
- Message filtering based on logical “Areas”
- Open world games, or simulations where clients are in different scenes
- Networked object ownership transfer
- Easily hook up your own functionality for:
- - Storing game state when clients disconnect
- - Authorizing player connections based on external systems
- - Server-side gameplay logic
…and more! 

**Client Server:**

[Source code](https://gitlab.com/dcarrigg/unity-clientservernetwork)

[Documentation](https://clientservernetwork.readthedocs.io/en/latest/)


**Peer to peer:**

[Source code](https://gitlab.com/dcarrigg/unity-p2pnetwork)

[Documentation](https://p2pnetwork.readthedocs.io/en/latest/)
