---
title: Speaking
subtitle: Selected Speaking and Media Engagements
comments: false
---

NHPR - May 11th, 2018 - A Video Game Odyssey: Pinball, Pong, & The True King of Kong

New Hampshire High Tech Council - Entrepreneur of the Year - June 13th 2017 - Game Development in NH

WMUR - May 12th, 2017 - Developing Video Games

SNHU - Janurary 26th 2016 - Programming with Time

Digital Portsmouth - The Art of Video Games - July 29 2015 - How I've Used Brilliant Tech to Make Mediocre Games

CS4NH - UNH Manchester - 11/5/2016 - Virtual Reality Workshop

Earn and Learn - July 2nd 2014 - Introduction to the Game Industry

Earn and Learn - November 19th 2014 - Introduction to the Game Industry

Earn and Learn - April 13th 2015 - Introduction to the Game Industry

IGDA NH - Feb 28, 2016 - The State of Virtual Reality

NHPR - FEB 27, 2013 - Video Game Industry Could Keep N.H. Young and Tech-Savvy

NHPR - OCT 2, 2015 - Can New Hampshire's Newbie Video Game Industry 'Level-Up'?

The Hippo - 07/09/15 - Level 2: New Hampshire video game developers have a new home

PortsmouthNH - July 22, 2015 - Indie developers want to make New Hampshire the next stage in gaming

New Hampshire High Tech Council: Entrepreneur Forum - Oct 14 2015 - "Skydive" Presentation

New Hampshire High Tech Council: Software Forum - April 2nd 2015 - Grinding it Out: the Status of the Video Game Industry in New Hampshire

SNHU: New Directions - December 7th 2016 - The State of Virtual Reality

Beford High School - October 9th, 2015 - STEM Day Round Panel

WMUR - Pittsfield boy who received new heart creates video game thanks to Make-A-Wish, SNHU - https://www.wmur.com/article/pittsfield-boy-who-received-new-heart-creates-video-game-thanks-to-make-a-wish-snhu/23583059